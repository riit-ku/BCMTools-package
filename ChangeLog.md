# BCMTools-package

A package of BCMTools with related libraries.



## RELEASE HISTORY

---
- 2017-07-06 Version 1.0.1
  - BCMTools-1.0.5
  - Cutlib-3.4.6
  - TextParser-1.8.5
  - Polylib-3.6.7
  - modify install.sh
  - add ChangeLog.md

  
---
- 2017-04-05 Version 1.0.0
  - BCMTools-1.0.4
  - Cutlib-3.4.4
  - TextParser-1.8.2
  - Polylib-3.6.5
  - Readme.md


