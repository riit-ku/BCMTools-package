# BCMTools package

Release package of BCMTools and related libraries.

## Copyright
- Copyright (c) 2017 Research Institute for Information Technology(RIIT), Kyushu University. All rights reserved.
- Copyright (c) 2015-2016 Advanced Institute for Computational Science(AICS), RIKEN. All rights reserved.


## Prerequisite

- Cmake
- MPI library
- TextParser (included in this package)
- TextParser (included in this package)
- Cutlib (included in this package)
- Polylib (included in this package)
- HDF5 (option)
- Zlib (option)
- Szip (option)
- Silo (option)


## Install
Type install shell script on command line with options .

~~~
$ export CC=... CXX=... F90=... FC=...
$ ./install.sh <intel|fx10|K|intel_F_TCS> <INST_DIR> {serial|mpi} {double|float}
~~~


## Supported platforms and compilers

* Linux Intel arch., Intel/GNU compilers.
* Mac OSX, Intel/GNU compilers.
* Sparc fx arch. and Fujitsu compiler.



## Contributors

- Kenji Ono *keno@cc.kyushu-u.ac.jp*


