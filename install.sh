#! /bin/sh
#
##############################################################################
#
# BCMTools package
#
# Copyright (C) 2017 Research Institute for Information Technology(RIIT), Kyushu University.
# All rights reserved.
#
# Copyright (c) 2015-2016 Advanced Institute for Computational Science, RIKEN.
# All rights reserved.
#

#######################################
#
# Library version
#

export TP_LIB=TextParser-1.8.5
export PL_LIB=Polylib-3.6.7
export CT_LIB=Cutlib-3.4.6
export BT_LIB=BCMTools-1.0.5



#######################################
#
# Usage
#
# $ ./install.sh <intel|fx10|K> <INST_DIR> {serial|mpi} {double|float}
#
#######################################


# Target machine
#
target_arch=$1

if [ "${target_arch}" = "intel" ]; then
  echo "Target arch       : intel"
elif [ "${target_arch}" = "fx10" ]; then
  echo "Target arch       : fx10"
  sparcv9="yes"
  toolchain_file="../cmake/Toolchain_fx10.cmake"
elif [ "${target_arch}" = "K" ]; then
  echo "Target arch       : K Computer"
  sparcv9="yes"
  toolchain_file="../cmake/Toolchain_K.cmake"
else
  echo "Target arch       : not supported -- terminate install process"
  exit
fi


# Install directory
#
target_dir=$2

export INST_DIR=${target_dir}
echo "Install directory : ${INST_DIR}"


# Parallelism
#
parallel_mode=$3

if [ "${parallel_mode}" = "mpi" ]; then
  echo "Paralle mode.     : ${parallel_mode}"
  mpi_sw="yes"
elif [ "${parallel_mode}" = "serial" ]; then
  echo "Paralle mode.     : ${parallel_mode}"
  mpi_sw="no"
else
  echo "Paralle mode.     : Invalid -- terminate install process"
  exit
fi


# Floating point Precision
#
fp_mode=$4

if [ "${fp_mode}" = "double" ]; then
  export PRCSN=double
elif [ "${fp_mode}" = "float" ]; then
  export PRCSN=float
else
  echo "Invalid argument for floating point  -- terminate install process"
  exit
fi

echo "Precision         : ${fp_mode}"
echo " "


#######################################


# TextParser
#
echo
echo -----------------------------
echo Install TextParser
echo
if [ ! -d ${TP_LIB} ]; then
  tar xvzf ${TP_LIB}.tar.gz
  mkdir ${TP_LIB}/build
  cd ${TP_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -Dwith_MPI=${mpi_sw} ..

  elif [ "${sparcv9}" = "yes" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=${mpi_sw} ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${TP_LIB} is already exist. Skip compiling."
fi



# Polylib
#
echo
echo -----------------------------
echo Install Polylib
echo
if [ ! -d ${PL_LIB} ]; then
  tar xvzf ${PL_LIB}.tar.gz
  mkdir ${PL_LIB}/build
  cd ${PL_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/Polylib \
            -Dwith_MPI=${mpi_sw} \
            -Dreal_type=${PRCSN} \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser ..

  elif [ "${sparcv9}" = "yes" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/Polylib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=${mpi_sw} \
            -Dreal_type=${PRCSN} \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${PL_LIB} is already exist. Skip compiling."
fi



# Cutlib
#
echo
echo -----------------------------
echo Install Cutlib
echo
if [ ! -d ${CT_LIB} ]; then
  tar xvzf ${CT_LIB}.tar.gz
  mkdir ${CT_LIB}/build
  cd ${CT_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/Cutlib \
            -Denable_OPENMP=no \
            -Denable_timing=no \
            -Denable_debug=no \
            -Dreal_type=float \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PL=${INST_DIR}/Polylib ..

  elif [ "${sparcv9}" = "yes" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/Cutlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Denable_OPENMP=no \
            -Denable_timing=no \
            -Denable_debug=no \
            -Dreal_type=float \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PL=${INST_DIR}/Polylib ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${CT_LIB} is already exist. Skip compiling."
fi




# BCMTools
#
echo
echo -----------------------------
echo BCMTools
echo
if [ ! -d ${BT_LIB} ]; then
  tar xvzf ${BT_LIB}.tar.gz
  mkdir ${BT_LIB}/build
  cd ${BT_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/BCMTools \
            -Dwith_MPI=${mpi_sw} \
            -Denable_OPENMP=yes \
            -Dreal_type=${PRCSN} \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PL=${INST_DIR}/Polylib \
            -Dwith_CUT=${INST_DIR}/Cutlib \
            -Dwith_HDF5=/usr/local/hdf5 \
            -Dwith_SILO=/usr/local/silo \
            -Denable_LARGE_BLOCK=no ..

  elif [ "${sparcv9}" = "yes" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/BCMTools \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=${mpi_sw} \
            -Denable_OPENMP=yes \
            -Dreal_type=${PRCSN} \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PL=${INST_DIR}/Polylib \
            -Dwith_CUT=${INST_DIR}/Cutlib \
            -Dwith_HDF5=/usr/local/hdf5 \
            -Dwith_SILO=/usr/local/silo \
            -Denable_LARGE_BLOCK=no ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${BT_LIB} is already exist. Skip compiling."
fi

